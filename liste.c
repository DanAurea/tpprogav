#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <liste.h>
#include <stdarg.h>
#include <individu.h>

/*
 * VARIABLE LOCALE
 */

unsigned long int liste_cpt = 0 ; 

/*
 * Methodes d'acces 
 */

/* Nombre d'elements */

extern 
int liste_nb_lire( liste_t * const liste )
{
  return(liste->nb );
} 

/* -- Acces individuel a un element */

extern 
void * liste_elem_lire( liste_t * const liste  , const int ind )
{
  if( (ind < 0) || (ind > liste_nb_lire(liste)-1 ) )
    {
      fprintf( stderr , "liste_elem_lire: mauvais indice d'element [%d] (devrait etre dans [%d..%d])\n",
	       ind , 0 , liste_nb_lire(liste)-1 );
      return(NULL);
    }

  return( liste->liste[ind] ) ;
}

/*
 * Tests 
 */

extern 
booleen_t liste_existe( liste_t * const liste )
{
  if( liste == NULL )
    {
      return(FAUX) ; 
     }
  else
    {
      return(VRAI) ; 
    }
}

extern 
booleen_t liste_vide( liste_t * const liste )
{
  if( liste->nb == 0 )
    {
      return(VRAI) ; 
    }
  else
    {
      return(FAUX) ; 
    }
}

/*
 * Methodes d'affectation 
 */


/* -- Acces individuel a un element */
extern 
err_t liste_elem_ecrire( liste_t * liste , 
			 void * const elem ,
			 const int ind ) 
{

#ifdef _DEBUG_
  if( (ind < 0) || (ind > liste_nb_lire(liste) ) )
    {
      fprintf( stderr , "liste_elem_ecrire: mauvais indice d'element [%d] (devrait etre dans [%d..%d]\n",
	       ind , 0 , liste_nb_lire(liste) );
      
      return(ERR_LISTE_IND_ELEM);
    }
#endif
  liste->affecter(liste->liste + ind, elem); /* Affecte l'élément en utilisant la fonction passée en paramètre lors de la création de la liste */

  return(OK) ;
}


/*
 * Creation d'une liste 
 */
extern
liste_t * liste_creer( const int nb, err_t (* affecter)(void * const, void * const), err_t (* detruire)(void * const) ){
  liste_t * liste ;
  
  if(( liste= malloc(sizeof(liste_t))) == NULL )
    {
      fprintf( stderr , "liste_creer: debordement memoire lors de la creation d'une liste\n");
      return((liste_t *)NULL);
    }
	
  liste->affecter = affecter; /* Pointeur sur la fonction d'affectation des objets */
  liste->detruire = detruire; /* Pointeur sur la fonction de destruction des objets */

  liste->nb = nb ;
  liste->liste = (void**)NULL ;
  if( nb > 0 ) {
    if( ( liste->liste = malloc( sizeof(void *) * nb ) ) == NULL ) {
  	  fprintf( stderr , "liste_creer: debordement memoire lors de la creation d'une liste\n");
  	  free( liste ) ;
  	  return((liste_t *)NULL);
    }
  }

  liste_cpt++ ; 

  return(liste);
}

/*
 * Destruction d'une liste 
 *
 */

extern
err_t liste_detruire( liste_t ** liste )
{
  int i;
  
  if(liste_existe(*liste)){ /* Vérifie l'existence de la liste */

    for(i = 0; i < (**liste).nb; i++){

      if(((**liste).liste + i) != NULL)
        (**liste).detruire((**liste).liste + i ); /* Détruit l'objet courant */

    }
   
    free((*liste)->liste); /* Libère la liste des éléments */
    (*liste)->liste = NULL; /* Réinitialise à NULL */
    free(*liste); /* Libère la mémoire pour la liste */
    *liste = NULL; /* Réinitialise */
   
    liste_cpt--;
  }

  return(OK) ;
}


/*
 * Affichage d'une liste sur la sortie standard
 *
 *
 */

extern 
void liste_afficher( liste_t * const liste , const char separateur, void (* afficher)(void *))
{
  int i;
	if(liste_existe(liste)){ /* Vérifie l'existence de la liste */

	 	for(i = 0; i < (*liste).nb; i++){
	 		
			if((*liste).liste[i] != NULL){ /* Vérifie que la liste n'est pas NULL */
	 		
				afficher((*liste).liste[i]); /* Affiche l'objet courant */
				printf("%c", separateur); /* Affiche le séparateur entre chaque élément */
	
	 		}
	 	}
 
	}
}

/**
 * Tri bulle d'une liste
 */
static err_t liste_trier_bulle(liste_t const * const liste, int (*comparer)(const void * const, const void * const), type_order_t order){
  int i, j, cmp = 0;
  void * tmp = NULL;

  for(i = (liste->nb - 1); i > 0; i--){ /* Parcours du tableau */
    for(j = 1; j <= i; j++){

      cmp = comparer(liste->liste + j-1, liste->liste + j); /* Compare les deux éléments génériques */

      if((order == DESC && cmp < 0) || (order == ASC && cmp > 0)){ /* Trie les valeurs selon l'ordre choisis */
      
          tmp = liste->liste[j-1]; /* Inversion éléments */
          liste->liste[j-1] = liste->liste[j];
          liste->liste[j] = tmp;
        
      }

    }
  }
    
  return OK;
}

/**
 * Tri qsort d'une liste
 */
static err_t liste_trier_qsort(liste_t * const liste, int (*comparer)(const void *, const void *)){ 
  
  qsort(liste->liste, (size_t)liste->nb, sizeof(void *), comparer);
  
  return OK;
}

/*
 * Tri d'une liste  
 *
 */

extern
err_t liste_trier( liste_t * const liste , int (* comparer)(const void ** const, const void ** const), int nbArg, ...){
  va_list args;
  type_tri_t type =  QUICK;
  type_order_t order = ASC;

  if(nbArg > 0){
    va_start(args, nbArg); /* Début de la pile d'arguments */
    
    type  = va_arg(args, type_tri_t); /* Récupère le type de tri */
    
    if(nbArg == 2){
      order = va_arg(args, type_order_t); /* Récupère le sens du tri */
    }
    else{ 
      fprintf(stderr, "Le nombre d'arguments est trop important ! Maximum 1 argument en surnombre !\n \
        1er arg: Type de tri, 2ème arg: Sens du tri");
      exit(EXIT_FAILURE);
    }

    va_end(args); /* Fin de la pile */
  }

  switch(type){ /* Tri choisis par l'utilisateur*/
    
    case QUICK:
      return liste_trier_qsort(liste, (void*) comparer);
      break;

    case PERSO:
      return liste_trier_bulle(liste, (void*) comparer, order);
      break;
    
    default:
      return ERR_SORT_TYPE; /* Erreur type de tri */
      break;
  }

  return(OK) ; 
}
