#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

#include <individu.h>
#include <fraction.h>
#include <mystring.h>
#include <liste.h>

/**
 * Fonction d'aide utilisation du programme
 * @param program Nom du programme
 */
void help(char * program){
	fprintf(stderr, "%s Common usage: Object-number [--verbose] [--tri= [0-1]] [--order=(ASC | DESC)]\n", program);
}

int
main(int argc, char * argv[] ) 
{
  err_t noerr = OK; 

  individu_t ** individus = NULL  ; 
  fraction_t ** fractions = NULL  ;
  string_t ** strings = NULL  ; 

  liste_t * liste = NULL ; 
  int i = 0, oc, optIndex , nbObj = 0;
  static int v;
  type_tri_t type = QUICK; /* Default type */
  type_order_t order = ASC; /* Default order */


  static struct option longOpt[] = { /* Define any long options */
    {"verbose", no_argument, &v, 'v'}, /* Verbose mode */
    {"tri", required_argument, NULL, 't'},
    {"order", required_argument, NULL, 'o'},
    {0, 0, 0, 0}
	};
	
	while((oc = getopt_long(argc, argv, "vt:o:", longOpt, &optIndex)) != -1){ /* Read each options passed through terminal */
		switch(oc){
			case 'v': /* Enable verbose if defined */
				v = 1;
			  break;

      case 't':
        if(atoi(optarg) != 0 && atoi(optarg) != 1){
          
          help(argv[0]);
          exit(EXIT_FAILURE);

        }else type = atoi(optarg);
			  break;

      case 'o':
        if(strcmp(optarg, "ASC") == 0 && type == PERSO) /* Set sort's order */
          order = ASC;
        else if(strcmp(optarg, "DESC") == 0 && type == PERSO)
          order = DESC;
        else{
          if(type != PERSO) fprintf(stderr, "Can't use order option with t0\n");
          help(argv[0]);
          exit(EXIT_FAILURE);
        }

        break;

			case '?':
				help(argv[0]);
				exit(EXIT_FAILURE);
				break;
			
			default:
				help(argv[0]);
				exit(EXIT_FAILURE);
				break;
		}
	}

	if(argc > 1 && argc < 6){ /* Check if too much or not enough args are passed */
		
		if(argc - optind == 1)
			nbObj = atoi(argv[optind]); /* Convert string arg to integer */
		else{
			help(argv[0]);
			exit(EXIT_FAILURE);
		}
		
	}else{
		help(argv[0]);
    exit(EXIT_FAILURE);
	}
  
  
  
  individus = malloc( sizeof(individu_t *) * nbObj )  ; 
  fractions = malloc( sizeof(fraction_t *) * nbObj )  ;
  strings = malloc( sizeof(string_t *) * nbObj )   ; 
	
	if( v ){ /* Only in verbose mode */
		printf( "Debut du programme des test sur les listes de %d objets homogenes\n" , nbObj ) ; 

		printf( "\nTest creation d'une liste de %d individus \n" , nbObj ) ;
 	}
  char prenom[128] ;
  char nom[128] ; 
  liste = liste_creer(nbObj, call_individu_copier, call_individu_detruire) ;  
  for( i=0 ; i<nbObj ; i++ ) 
    {
      sprintf( nom , "nom_%d" , nbObj-i ) ;
      sprintf( prenom , "prenom_%d" , nbObj-i ) ;
      individus[i] = individu_creer( prenom , nom ) ; 
      liste_elem_ecrire( liste , individus[i] , i ) ;
    }
	
	if(v) /* Only in verbose mode */
  	printf( "Test affichage liste d'individus AVANT tri \n" ) ;
  liste_afficher( liste , ' ', (void *) individu_afficher ) ; /* Display list with function's pointer casted by void * to avoid warning */
  printf( "\n");
	
	if(v) /* Only in verbose mode */
  	printf( "Test Tri de la liste des individus\n" );
  liste_trier( liste, call_individu_comparer, 2, type, order) ;
	
	if(v) /* Only in verbose mode */
  	printf( "Test affichage liste d'individus APRES tri\n" ) ;
  liste_afficher( liste , ' ', (void *) individu_afficher ) ; /* Display list with function's pointer casted by void * to avoid warning */
  printf( "\n");
 
 	if(v) /* Only in verbose mode */
  	printf( "Test destruction liste d'individus\n" ) ;
  if( ( noerr = liste_detruire( &liste) ) )
    { 
      printf("Sortie avec code erreur = %d\n" , noerr ) ;
      return(noerr) ; 
    }
	
	if(v) /* Only in verbose mode */
  	printf( "\nTest creation d'une liste de %d fractions \n" , nbObj ) ;
  liste = liste_creer(nbObj, call_fraction_referencer, call_fraction_effacer) ;  
  for( i=0 ; i<nbObj ; i++ ) 
    {
      fractions[i] = fraction_creer( nbObj-i , nbObj-i+1 ) ; 
      liste_elem_ecrire( liste , fractions[i] , i ) ;
    }
	
	if(v) /* Only in verbose mode */
  	printf( "Test affichage liste de fractions AVANT tri\n" ) ;
  liste_afficher( liste , ' ', (void *) fraction_afficher ) ; /* Display list with function's pointer casted by void * to avoid warning */
  printf( "\n");
	
	if(v) /* Only in verbose mode */
  	printf( "Test Tri de la liste des fractions\n" );
  liste_trier( liste, call_fraction_comparer, 2, type, order) ;
	
	if(v) /* Only in verbose mode */
  	printf( "Test affichage liste des fractions APRES tri\n" ) ;
  liste_afficher( liste ,  ' ', (void *) fraction_afficher ) ; /* Display list with function's pointer casted by void * to avoid warning */
  printf( "\n");
 	
 	if(v) /* Only in verbose mode */
  	printf( "Test destruction liste de fractions\n" ) ;
  if( ( noerr = liste_detruire( &liste) ) )
    { 
      printf("Sortie avec code erreur = %d\n" , noerr ) ;
      return(noerr) ; 
    }
  
	if(v) /* Only in verbose mode */
  	printf( "\nTest creation d'une liste de %d strings \n" , nbObj ) ;
  char string[128] ;
  liste = liste_creer(nbObj, call_string_referencer, call_string_effacer) ;  
  for( i=0 ; i<nbObj ; i++ ) 
    {
      sprintf( string , "string_%d" , nbObj-i ) ; 
      strings[i] = string_creer( string ) ; 
      liste_elem_ecrire( liste , strings[i] , i ) ;
    }
	
	if(v) /* Only in verbose mode */
  	printf( "Test affichage liste de strings AVANT tri\n" ) ;
  liste_afficher( liste ,  ' ', (void *) string_afficher ) ; /* Display list with function's pointer casted by void * to avoid warning */
  printf( "\n");
 
 	if(v) /* Only in verbose mode */
  	printf( "Test Tri de la liste des strings\n" );
  liste_trier( liste, call_string_comparer, 2, type, order) ;
  
  if(v) /* Only in verbose mode */
  	printf( "Test affichage liste des strings APRES tri\n" ) ;
  liste_afficher( liste ,  ' ', (void *)string_afficher ) ; /* Display list with function's pointer casted by void * to avoid warning */
  printf( "\n");
  
  if(v) /* Only in verbose mode */
  	printf( "Test destruction liste de strings\n" ) ;
  if( ( noerr = liste_detruire( &liste  ) ) )
    { 
      printf("Sortie avec code erreur = %d\n" , noerr ) ;
      return(noerr) ; 
    }

  for(i = 0; i < nbObj; i++){
    individu_detruire(individus + i);
    fraction_detruire(fractions + i);
    string_detruire(strings + i);
  }

  free( individus ) ;
  free( fractions ) ;
  free( strings ) ; 

  printf( "\nFin du programme des test sur la lste d'objets homogenes\n" ) ; 
  
  printf( "Nombre de liste_t  = %lu\n" , liste_cpt ) ;

  return(0) ; 
}
