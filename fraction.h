#ifndef _FRACTION_H_
#define _FRACTION_H_

#include <commun.h>

/*
 * DEFINITION OBJET "INDIVIDU"  
 */

typedef struct fraction_s fraction_t;

struct fraction_s 
{
  int numerateur ;
  int denominateur ;
};

/*
 * VARIABLE LOCALE
 * declaration du compteur d'objets fraction_t comme variable externe
 * pour compter les references sur ces objets 
 */

extern unsigned long int fraction_cpt  ;

/* 
 * FONCTIONS
 */

/* Creation */
extern fraction_t * fraction_creer(const int numerateur , const int denominateur ) ;
/* Test existance */
extern booleen_t fraction_existe( fraction_t * const fraction ) ;
/* Affichages */
extern void fraction_afficher( fraction_t * const fraction ) ;
/* Comparaisons */
extern int fraction_comparer( const fraction_t * const f1 , const fraction_t * const f2 ) ;
extern int call_fraction_comparer(const void ** const frac1, const void ** const frac2);

/* Affectation par copie des fractions */
extern err_t fraction_copier(fraction_t ** const, fraction_t * const);
extern err_t call_fraction_copier(void * const, void * const);

/* Affectation par référencement des fractions */
extern err_t fraction_referencer(fraction_t ** const, fraction_t * const);
extern err_t call_fraction_referencer(void * const, void * const);

/* Destructions */
extern err_t fraction_detruire( fraction_t ** fraction ) ;
extern err_t call_fraction_detruire(void * const fraction);

extern err_t fraction_effacer(fraction_t ** fraction);
extern err_t call_fraction_effacer(void * const);

#endif
