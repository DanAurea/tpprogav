#ifndef _INDIVIDU_H_
#define _INDIVIDU_H_

/*
 * DEFINITION OBJET "INDIVIDU"  
 */

#include <commun.h>

typedef struct individu_s 
{
  char * nom ;
  char * prenom ;
} individu_t ;

/*
 * VARIABLE LOCALE
 * declaration du compteur d'objets individu_t comme variable externe
 * pour compter les references sur ces objets 
 */

extern unsigned long int individu_cpt  ;

/* 
 * FONCTIONS
 */

extern individu_t * individu_creer(char * const prenom , char * const nom ) ;
extern booleen_t individu_existe( individu_t * const individu ) ;
extern void individu_afficher( individu_t * const individu ) ;
extern int individu_comparer( const individu_t * const ind1 , const individu_t * const ind2 ) ;
extern int call_individu_comparer(const void ** const indiv1, const void ** const indiv2);

/* Affectation par copie des individus */
extern err_t individu_copier(individu_t ** const, individu_t * const);
extern err_t call_individu_copier(void * const, void * const);

/* Affectation par référencement des individus */
extern err_t individu_referencer(individu_t ** const, individu_t * const);
extern err_t call_individu_referencer(void * const, void * const);

/* Destruction d'une copie d'individu */
extern err_t individu_detruire( individu_t ** individu );
extern err_t call_individu_detruire( void * const);

extern err_t individu_effacer(individu_t ** individu);
extern err_t call_individu_effacer(void * const);

#endif
