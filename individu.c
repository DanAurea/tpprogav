#include <stdlib.h>
#include <string.h>
#include <individu.h>


/*
 * VARIABLE LOCALE
 */

unsigned long int individu_cpt = 0 ; 

/* 
 * FONCTIONS
 */

extern 
booleen_t individu_existe( individu_t * const individu )
{
  if( individu == NULL ) 
    return(FAUX) ;
  else
    return(VRAI) ; 
}

extern
void individu_afficher( individu_t * const individu ) 
{

  printf( "{" ) ; 
  if(  individu_existe(individu) ) 
    {
      printf( "%s %s" , individu->prenom , individu->nom ) ;
    }
  printf( "}" ) ; 
}

extern 
individu_t * individu_creer( char * const prenom , char * const nom ) 
{
  individu_t * individu = NULL ; 

  /* Creation place memoire objet individu */
  if( ( individu = malloc(sizeof(individu_t)) ) == NULL )
    {
      fprintf( stderr , "individu_creer: debordement memoire lors de la creation d'un objet de type individu_t (%lu demandes)\n", 
	       (unsigned long int)sizeof(individu_t) ) ;
      return((individu_t *)NULL);
    }

  /* Affectation attributs specifiques */
  if( ( individu->nom = malloc( sizeof(char)*strlen(nom)+1) ) == NULL )
    {
      fprintf( stderr , "individu_creer: debordement memoire lors de la creation du nom d'un individu_t (%lu demandes)\n", 
	       (unsigned long int)sizeof(char)*strlen(nom)+1 ) ;
      return((individu_t *)NULL);
    }

  if( ( individu->prenom = malloc( sizeof(char)*strlen(prenom)+1) ) == NULL )
    {
      fprintf( stderr , "individu_creer: debordement memoire lors de la creation du prenom d'un individu_t (%lu demandes)\n", 
	       (unsigned long int)sizeof(char)*strlen(prenom)+1 ) ;
      return((individu_t *)NULL);
    }

  strcpy( individu->nom , nom ); 
  strcpy( individu->prenom , prenom ) ;
  
  individu_cpt++ ; 

  return( individu ) ;
}

extern 
int individu_comparer( const individu_t * const ind1 , const individu_t * const ind2 ) 
{
  int cmp = strcmp( ind1->nom , ind2->nom )  ;
  if( cmp ) return(cmp); 
  return( strcmp( ind1->prenom , ind2->prenom ) ) ;
}

extern
int call_individu_comparer(const void ** const indiv1, const void ** const indiv2)
{
  return individu_comparer(* indiv1, * indiv2);
}

/**
 * Affectation par référencement d'individu
 * @param  dest Destination
 * @param  elem Elem à référencer
 * @return      Retourne code d'erreur
 */
extern err_t individu_referencer(individu_t ** const dest, individu_t * const elem){
  * dest = elem;

  return OK;
}

/**
 * Fonction d'encapsulation
 * @param  dest Destination
 * @param  elem Elem à référencer
 * @return       Retourne code d'erreur
 */
extern err_t call_individu_referencer(void * const dest, void * const elem){
  return individu_referencer(dest, elem);
}

/**
 * Affectation par copie d'individu
 * @param  dest Destination
 * @param  elem Elem à copier
 * @return      Retourne code d'erreur
 */
extern err_t individu_copier(individu_t ** const dest, individu_t * const elem){
  * dest = malloc(sizeof( individu_t));

  if(* dest == NULL) /* Débordement mémoire */
    return ERR_DEB_MEMOIRE;

  (** dest).nom = malloc( sizeof(char) * (strlen((*elem).nom) + 1));
  if((** dest).nom == NULL){ /* Débordement mémoire */
    
    free(* dest);
    * dest = NULL;
    return ERR_DEB_MEMOIRE;
    
  }

  strcpy((** dest).nom, (* elem).nom); /* Recopie le nom d'élem dans destination */

  (** dest).prenom = malloc( sizeof(char) * (strlen((* elem).prenom) + 1));
  if((**dest).prenom == NULL){ /* Débordement mémoire */
    
    free((** dest).nom);
    (** dest).nom = NULL;
    free(* dest);
    * dest = NULL;

    return ERR_DEB_MEMOIRE;
  }

  strcpy((** dest).prenom, (* elem).prenom); /* Recopie le prénom d'élem dans destination */

  return OK;
}

/**
 * Fonction d'encapsulation
 * @param  dest Destination
 * @param  elem Elem à copier
 * @return       Retourne code d'erreur
 */
extern err_t call_individu_copier(void * const dest, void * const elem){
  return individu_copier(dest, elem);
}

/**
 * Destruction d'une copie d'individu
 * @param individu Individu à détruire
 * @return         Retourne code d'erreur
 */
extern err_t individu_detruire( individu_t ** individu ) {
  if(*individu != NULL){ /* Vérifie que l'objet existe bien */

    if((**individu).nom != NULL){ /* Si l'objet a un nom on le détruit */
      free((**individu).nom);
      (**individu).nom = NULL; /* Réinitialise */
    }
    
    if((**individu).prenom != NULL){ /* Si l'objet a un prénom on le détruit */
      free((**individu).prenom);
      (**individu).prenom = NULL; /* Réinitialise */
    }
    
    free(*individu); /* Détruit l'objet */
    *individu = NULL; /* Réinitialise */
    
    individu_cpt--; /* Décrémente le compteur sur individu */
  }
  
  return(OK) ; 
}

/**
 * Fonction d'encapsulation
 * @param  individu Individu à détruire
 * @return          Retourne code d'erreur
 */
extern err_t call_individu_detruire(void * const individu){
  return individu_detruire(individu);
}

extern err_t individu_effacer(individu_t ** individu){
    if(* individu !=  NULL){
      * individu = NULL;

      individu_cpt--;
    }

    return OK;
}

extern err_t call_individu_effacer(void * const individu){
  return individu_effacer(individu);
}