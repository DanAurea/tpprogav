#ifndef _MYSTRING_H_
#define _MYSTRING_H_

#include <commun.h>

/*
 * DEFINITION OBJET "STRING"  
 */

typedef struct string_s 
{
  char * string ;
} string_t ;

/*
 * VARIABLE LOCALE
 * declaration du compteur d'objets string_t comme variable externe
 * pour compter les references sur ces objets 
 */

extern unsigned long int string_cpt  ;

/* 
 * FONCTIONS
 */

extern string_t * string_creer(char * const chaine ) ;
extern booleen_t string_existe( string_t * const string ) ;
extern void string_afficher( string_t * const string ) ;
extern int string_comparer( const string_t * const str1 , const string_t * const str2 ) ;
extern int call_string_comparer(const void ** const string1, const void ** const string2);

/* Affectation par copie des chaines */
extern err_t string_copier(string_t ** const, string_t * const);
extern err_t call_string_copier(void * const, void * const);

/* Affectation par référencement des chaines */
extern err_t string_referencer(string_t ** const, string_t * const);
extern err_t call_string_referencer(void * const, void * const);

extern err_t string_detruire( string_t ** string );
extern err_t call_string_detruire(void * const string);

extern err_t string_effacer(string_t ** string);
extern err_t call_string_effacer(void * const);

#endif