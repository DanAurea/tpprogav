#include <stdlib.h>
#include <string.h>
#include <mystring.h>


/*
 * VARIABLE LOCALE
 */

unsigned long int string_cpt = 0 ; 

/* 
 * FONCTIONS
 */

extern 
booleen_t string_existe( string_t * const string )
{
  if( string == NULL ) 
    return(FAUX) ;
  else
    return(VRAI) ; 
}

extern
err_t string_detruire( string_t ** string ) 
{
  if(*string != NULL){ /* Vérifie si l'objet existe*/
    
    if((**string).string != NULL){ /* Vérifie si la chaîne existe */
    
    free((**string).string); /* Libère la chaine */
    (**string).string = NULL; /* Réinitialise */
    }
    
    free(*string); /* Libère l'objet */
    *string = NULL; /* Réinitialise */
    
    string_cpt--; /* Décrémente le compteur sur string_t */
  }
  
  return(OK) ; 
}

extern err_t call_string_detruire(void * const string){
  return string_detruire(string);
}

extern err_t string_effacer(string_t ** string){
  if( * string != NULL){
    * string = NULL;

    string_cpt--;
  }

  return OK;
}

extern err_t call_string_effacer(void * const string){
  return string_effacer(string);
}

extern
void string_afficher( string_t * const string ) 
{

  printf( "{" ) ; 
  if(  string_existe(string) ) 
    {
      printf( "%s" , string->string) ;
    }
  printf( "}" ) ; 
}

extern 
string_t * string_creer( char * const chaine ) 
{
  string_t * string = NULL ; 

  /* Creation place memoire objet string */
  if( ( string = malloc(sizeof(string_t)) ) == NULL )
    {
      fprintf( stderr , "string_creer: debordement memoire lors de la creation d'un objet de type string_t (%lu demandes)\n", 
	       (unsigned long int)sizeof(string_t) ) ;
      return((string_t *)NULL);
    }

  /* Affectation attributs specifiques */
  if( ( string->string = malloc(strlen(chaine)+1) ) == NULL )
    {
      fprintf( stderr , "string_creer: debordement memoire lors de la creation d'un objet de type string_t (%lu demandes)\n", 
	       (unsigned long int)strlen(chaine)+1 ) ;
      return((string_t *)NULL);
    }
  strcpy( string->string , chaine ) ;

  string_cpt++ ; 

  return( string ) ;
}

extern
int string_comparer( const string_t * const str1 , const string_t * const str2 )
{
  return(strcmp( str1->string , str2->string) ) ;
}

extern
int call_string_comparer(const void ** const string1, const void ** const string2)
{
  return string_comparer(* string1, *string2);
}

/**
 * Affectation d'une chaîne par référencement
 * @param  dest Destination
 * @param  elem Elem à référencer
 * @return      Retourne code d'erreur
 */
extern err_t string_referencer(string_t ** const dest, string_t * const elem){
  * dest = elem;

  return OK;
}

/**
 * Fonction d'encapsulation
 * @param  dest Destination
 * @param  elem Elem à référencer
 * @return      Retourne code d'erreur
 */
extern err_t call_string_referencer(void * const dest, void * const elem){
  return string_referencer(dest, elem);
}

/**
 * Affectation d'une chaîne par copie
 * @param  dest Destination
 * @param  elem Elem à copier
 * @return      Retourne code d'erreur
 */
extern err_t string_copier(string_t ** const dest, string_t * const elem){
  * dest = malloc(sizeof(string_t));

  if(* dest == NULL) /* Débordement mémoire */
    return ERR_DEB_MEMOIRE;

  (** dest).string = malloc(strlen((* elem).string) + 1);
  if((** dest).string == NULL){ /* Débordement mémoire */
    free(* dest);
    * dest = NULL;

    return ERR_DEB_MEMOIRE;
  }

  strcpy((**dest).string, (* elem).string);

  return OK;
}

/**
 * Fonction d'encapsulation
 * @param  dest Destination
 * @param  elem Elem à copier
 * @return      Retourne code d'erreur
 */
extern err_t call_string_copier(void * const dest, void * const elem){
  return string_copier(dest, elem);
}