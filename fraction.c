#include <stdlib.h>
#include <string.h>
#include <fraction.h>


/*
 * VARIABLE LOCALE
 */

unsigned long int fraction_cpt = 0 ; 

/* 
 * FONCTIONS
 */

extern 
booleen_t fraction_existe( fraction_t * const fraction )
{
  if( fraction == NULL ) 
    return(FAUX) ;
  else
    return(VRAI) ; 
}

extern
err_t fraction_detruire( fraction_t ** fraction ) 
{
  if(*fraction != NULL){ /* Vérifie si l'objet existe */
    
    free(*fraction); /* Libère la mémoire */
    *fraction = NULL; /* Réinitialise */
    
    fraction_cpt--; /* Décrémente le compteur sur fraction */
  }
  
  return(OK) ; 
}

extern err_t call_fraction_detruire(void * const fraction){
  return fraction_detruire(fraction);
}

extern err_t fraction_effacer(fraction_t ** fraction){
  if(* fraction != NULL){
    * fraction = NULL;

    fraction_cpt--;
  }

  return OK;
}

extern err_t call_fraction_effacer(void * const fraction){
  return fraction_effacer(fraction);
}

extern
void fraction_afficher( fraction_t * const fraction ) 
{

  printf( "{" ) ; 
  if(  fraction_existe(fraction) ) 
    {
      printf( "%d/%d" , fraction->numerateur , fraction->denominateur ) ;
    }
  printf( "}" ) ; 
}

extern 
fraction_t * fraction_creer( const int numerateur , const int denominateur ) 
{
  fraction_t * fraction = NULL ; 

  /* Creation place memoire objet fraction */
  if( ( fraction = malloc(sizeof(fraction_t)) ) == NULL )
    {
      fprintf( stderr , "fraction_creer: debordement memoire lors de la creation d'un objet de type fraction_t (%lu demandes)\n", 
	       (unsigned long int)sizeof(fraction_t) ) ;
      return((fraction_t *)NULL);
    }

  /* Affectation attributs specifiques */
  fraction->numerateur = numerateur; 
  fraction->denominateur = denominateur ;
  
  fraction_cpt++ ; 

  return( fraction ) ;
}

extern
int fraction_comparer( const fraction_t * const f1 , const fraction_t * const f2 ) 
{ 
  
  float r1 = (float) f1->numerateur / (float) f1->denominateur  ; 
  float r2 = (float) f2->numerateur / (float) f2->denominateur  ; 
  if( r1 == r2 ) return(0) ; 
  else if( r1 > r2 )  return(1) ; 
  else return(-1) ;   
}

extern
int call_fraction_comparer(const void ** const frac1, const void ** const frac2)
{
  return fraction_comparer(* frac1, * frac2);
}

/**
 * Affectation par référencement
 * @param  dest Destination
 * @param  elem Elém à référencer
 * @return      Retourne code d'erreur
 */
extern err_t fraction_referencer(fraction_t ** const dest, fraction_t * const elem){
  * dest = elem;

  return OK;
}

/**
 * Fonction d'encapsulation
 * @param  dest Destination
 * @param  elem Elem à référencer
 * @return      Retourne code d'erreur
 */
extern err_t call_fraction_referencer(void * const dest, void * const elem){
  return fraction_referencer(dest, elem);
}

/**
 * Affectation par copie
 * @param  dest Destination
 * @param  elem Elément à copier
 * @return       Retourne code d'erreur
 */
extern err_t fraction_copier(fraction_t ** const dest, fraction_t * const elem){

  * dest = malloc(sizeof(fraction_t)); /* Alloue de la mémoire pour la copie */

  if(* dest == NULL) /* Debordement mémoire */
    return ERR_DEB_MEMOIRE;

  /* Recopie des valeurs d'elem dans dest */
  (** dest).numerateur   = (* elem).numerateur;
  (** dest).denominateur = (* elem).denominateur;

  return OK;
}

/**
 * Fonction d'encapsulation
 * @param  dest Destination
 * @param  elem Elément à copier
 * @return       Retourne code d'erreur
 */
extern err_t call_fraction_copier(void * const dest, void * const elem){
  return fraction_copier(dest, elem);
}
